import * as tsj from "ts-json-schema-generator";
import * as path from "path";
import Ajv from "ajv";

const config = {
  path: path.join(__dirname, "../../events/**/*.ts"),
};

const schema = tsj.createGenerator(config).createSchema();

const ajv = new Ajv({ schemas: [schema] });

/**
 * Validate that an input matches the defined structure of an event
 *
 * @param event - The name of the event you want to publish. It must be an event exported by @hindawi/phenom-events
 * @param data - An object representing the data structure to validate against the event schema
 * @returns A boolean of weather the data matches the event schema
 * @throws An error with the validation errors for devs to be able to inspect their data
 */
export function validate(
  event: string,
  data: Record<string, unknown>,
): boolean {
  const isValid = ajv.validate(`#/definitions/${event}`, data);

  if (!isValid) {
    if (ajv.errors == null) {
      throw new Error('Invalid error')
    }
    const errors = ajv.errors.map((err) => `${err.instancePath} ${err.message}`);
    throw new Error(errors.join("\n"));
  }

  return isValid;
}
