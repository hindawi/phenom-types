import { ISODateString } from "./dateType";

export enum SyndicationTargetStatus {
  canceled = "canceled",
  failed = "failed",
  pending = "pending",
  pickedUp = "pickedUp",
  resolved = "resolved",
  running = "running",
  success = "success",
}

export interface CrossrefTarget {
  endDate: ISODateString;
  id: string;
  resolvedDate: ISODateString;
  startDate: ISODateString;
  status: CrossrefTargetStatus;
  type: CrossrefTargetType;
}

/**
 * Failed targets when resyndicated become resolved and a new target is created.
 */

export enum CrossrefTargetStatus {
  canceled = "canceled",
  apiFailed = "apiFailed",
  noIssn = "noIssn",
  pending = "pending",
  pickedUp = "pickedUp",
  resolved = "resolved",
  running = "running",
  apiSuccess = "apiSuccess",
  processingError = "processingError",
  processingSuccess = "processingSuccess",
  alreadySubmitted = "alreadySubmitted",
}

/**
 * Auto targets are created on article received events.
 * Manual targets are created when the user re-syndicates targets.
 */
export enum CrossrefTargetType {
  auto = "auto",
  manual = "manual",
}

export enum ArticleStatus {
  error = "error",
  resolved = "resolved",
  success = "success",
  unknown = "unknown",
}

export enum BatchItemType {
  auto = "auto",
  manual = "manual",
}

export interface Database {
  acceptanceCriteria: string;
  id: string;
  name: string;
}

export interface Publisher {
  name: string;
}

export interface Article {
  articleType?: BatchItemType;
  customId: string;
  status: ArticleStatus;
}

export interface SyndicationTarget {
  articles: Article[];
  endDate?: ISODateString;
  id: string;
  startDate: ISODateString;
  status: SyndicationTargetStatus;
}

export enum JournalDatabaseStatus {
  ceasedCoverage = "ceasedCoverage",
  covered = "covered",
  draft = "draft",
  rejected = "rejected",
  rejectedFinal = "rejectedFinal",
  reviewHold = "reviewHold",
  underReview = "underReview",
}
