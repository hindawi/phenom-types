import { ISODateString } from "./dateType";

export interface EventObject {
  created: ISODateString;
  id: string;
  updated: ISODateString;
}
