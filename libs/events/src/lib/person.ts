export interface Person {
  id: string;
  email: string;
  givenNames: string;
  surname: string;
}
