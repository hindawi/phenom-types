import { ISODateString } from "./dateType";

export interface Manuscript {
  submissionId: string;
  title: string;
  articleType: {
    /** The Wiley ArticleTypeHarmonization article type name (see https://confluence.wiley.com/display/RTDATA/Article+Type+Harmonization) */
    athName: string;
  };
  journal: Journal;
  customId: string;
  doi?: string;
  submittedDate: ISODateString;
  acceptedDate?: ISODateString
  /** 
   * acceptedDate is required in ManuscriptPostAcceptanceEeoLinkExportRequested 
   * acceptedDate value is
   * - submissionQualityCheckPassedDate for short article types
   * - peerReviewPassedDate for the rest
   */
  fundingStatement?: FundingStatement;
}

export interface FundingStatement {
  /** 
   * Note that this may contain the funding statement from TAs, which may or 
   * may not have overwritten the original funding statement entered by the
   * user. 
   * 
   * Currently, this logic of combining statements sits in the phenom-taacl service.
   * The production-package service should add this to the JATS XML as is.
   */
  plainText?: string,

  // Things like funderrefs/DOIs & structured grant numbers will be added later
}

export interface Journal {
  /** The Phenom UUID */
  id: string,
  email?: string;
  name: string;
  issn?: string;
  /** The journal code from STEP/Stibo, max 4 chars. This will be the same as the Phenom code except in case of conflicts or Phenom codes longer than 4 chars. */
  stepCode: string;
}

export interface Alias {
  surname: string;
  givenNames: string;
  /** The honorary title for this person, e.g. "Dr" */
  title?: string;
  email: string;
  affiliation: Affiliation;
}

export interface Affiliation {
  name: string,
  address?: string,
  department?: string,
  /** ISO-3166 country codes */
  countryCode?: string,

  // more things like ROR/Ringgold/WCM_ID to be added here
}

interface User {
  identities: Identity[];
}

export interface Identity {
  type: string;
  identifier: string;
  updated: ISODateString;
}

export interface Author {
  id: string;
  isCorresponding: boolean;
  alias: Alias;
  user: User;
}

export interface File {
  type: string;
  fileName: string;
  providerKey: string;
}

export enum PublicationChargeType {
  Personal = "Personal",
  Covered = "Covered",
  Waiver = "Waiver"
}
