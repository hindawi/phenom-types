import { AcademicPerson } from "./academicPerson";
import { EditorRole } from "./editorRole";
import { ISODateString } from "./dateType";
import { MemberStatuses } from "./memberStatuses";

export interface Editor extends AcademicPerson {
  acceptedDate: ISODateString;
  assignedDate: ISODateString;
  declinedDate: ISODateString;
  expiredDate: ISODateString;
  id: string;
  status: MemberStatuses;
  invitedDate: ISODateString;
  orcidId: string;
  removedDate: ISODateString;
  role: EditorRole;
  userId: string;

  /**
   * @param declineReason - Shows the reason why an editor declined to be the
   * academic editor of the manuscript
   */
  declineReason?: string | null;
}
