import { Checker, TeamLeader } from "./checker";

export enum AuthorProfileVerificationStatus {
  verifiedByWebOfScience = "verifiedByWebOfScience",
  verifiedOutOfTool = "verifiedOutOfTool",
  cantBeVerified = "cantBeVerified",
  notVerified = "notVerified",
}

export interface AuthorProfile {
  email: string;
  verificationStatus: AuthorProfileVerificationStatus;
  sanctionInfo: {
    isOnBadDebtList: boolean;
    isOnWatchList: boolean;
    isOnBlackList: boolean;
  };
}

export interface SuspiciousWord {
  word: string;
  occurrences: number;
}

export enum AutomaticCheckResultValue {
  succeeded = "succeeded",
  failed = "failed",
}

export interface AutomaticCheckResult<T> {
  result: AutomaticCheckResultValue;
  details: T;
}

export interface AutomaticCheckResultSucceded<T>
  extends AutomaticCheckResult<T> {
  result: AutomaticCheckResultValue.succeeded;
}

export interface AutomaticCheckResultFailed<T> extends AutomaticCheckResult<T> {
  result: AutomaticCheckResultValue.failed;
  reason: string;
}

export type AutomaticCheck<T> =
  | AutomaticCheckResultSucceded<T>
  | AutomaticCheckResultFailed<T>;

export interface SimilarSubmission {
  submissionId: string;
  customId: string;
  manuscriptId: string;
}

export interface LanguageCheckDetails {
  wordCount: number;
  languageMatchPercentage: number;
}

export interface SuspiciousCheckDetails {
  suspiciousWords: SuspiciousWord[];
}

export interface SimilarityCheckDetails {
  firstSourceSimilarityPercentage: number;
  totalSimilarityPercentage: number;
  similarityReportUrl: string;
}

export interface PreScreeningChecks {
  languageCheck: AutomaticCheck<LanguageCheckDetails>;
  suspiciousCheck: AutomaticCheck<SuspiciousCheckDetails>;
  similarityCheck: AutomaticCheck<SimilarityCheckDetails>;
  similarSubmissions: SimilarSubmission[];
}

export interface InfoValidation {
  title: {
    observations?: string;
  };
  abstract: {
    includesCitations: boolean;
    graphical: boolean;
    observations?: string;
  };
  authors: {
    differentAuthors: boolean;
    missingAffiliations: boolean;
    nonacademicAffiliations: boolean;
    observations?: string;
  };
  relevance: {
    topicNotAppropriate: boolean;
    articleTypeIsNotAppropriate: boolean;
    manuscriptIsMissingSections: boolean;
    observations?: string;
  };
  referenceExtraction: {
    missingReferences: boolean;
    wrongReferences: boolean;
    observations?: string;
  };
}

export interface ScreeningChecks {
  preScreening: PreScreeningChecks;
  infoValidation?: InfoValidation;
  authorProfiles: AuthorProfile[];
}

export interface ScreeningProcess {
  submissionId: string;
  manuscriptId: string;
  customId: string;
  checks: ScreeningChecks;
  checker: Checker;
  teamLeaders: TeamLeader[];
}
