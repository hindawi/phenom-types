import {
  PeerReviewCycleChecks,
  PeerReviewCycleCheckingProcess,
} from "./peerReviewCycleCheckingProcess";
import { Question } from "./question";

export interface MaterialChecks extends PeerReviewCycleChecks {
  material: {
    questions: Question[];
  };
}

export interface MaterialCheckingProcess
  extends PeerReviewCycleCheckingProcess {
  checks: MaterialChecks;
}
