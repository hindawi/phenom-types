export interface Question {
  name: string;
  selectedOption: string;
  observation?: string;
}
