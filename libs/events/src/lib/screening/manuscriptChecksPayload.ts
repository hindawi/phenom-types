interface Publication {
  manuscriptWosId: string;
}

interface CheckedAuthorProfile {
  displayName: string;
  fullName: string;
  wosStandard: string;
  firstName: string;
  lastName: string;
  email: string;
  manuscriptWosId: string;
  affiliation: string;
  manuscriptAbstract: string;
  publications: Publication[];
  noOfPublications: string;
  weight: number;
  authorId: string;
}

interface RorClosestMatch {
  id: string | null,
  aff: string | null,
  score: number | null
}

interface AuthorProfilesSearch {
  profiles: CheckedAuthorProfile[] | null;
  isVerified: boolean;
  hasOrcidIssue: boolean;
  ror: RorClosestMatch
}

interface SanctionListAuthor {
  [uuid: string]: {
    isOnBadDebtList: boolean,
    isOnBlackList: boolean,
    isOnWatchList: boolean,
  };
}

export interface AuthorProfilesSearchPayload {
  manuscriptId: string;
  authors: AuthorProfilesSearch[];
}

export interface AutomaticChecksDonePayload {
  manuscriptId: string;
}

export interface FileConvertedPayload {
  fileId: string;
  manuscriptId: string;
  convertedFileProviderKey: string;
  textKey: string;
}

export interface LanguageCheckedPayload {
  manuscriptId: string;
  englishPercentage: number;
  wordCount: number;
}

export interface SanctionListCheckDonePayload {
  manuscriptId: string;
  authors: SanctionListAuthor[];
}

export interface SuspiciousWordsProcessingDonePayload {
  manuscriptId: string;
  suspiciousWords: {
    [key: string]: {
      reason: string,
      count: number,
    },
  };
}

export interface SimilarityCheckFinishedPayload {
  manuscriptId: string;
  totalSimilarityPercentage: number;
  firstSourceSimilarityPercentage: number;
  reportUrl: string;
}
