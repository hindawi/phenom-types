import { ScreeningChecks, ScreeningProcess } from "./screeningProcess";
import { Question } from "./question";

interface UserIdentifier {
  id: string;
}

enum ConflictOfInterestType {
  academicEditor = "academicEditor",
  triageEditor = "triageEditor",
  reviewers = "reviewers",
  none = "none",
}

interface ImproperReview {
  reviewers: UserIdentifier[];
}

interface ConflictOfInterest {
  type: ConflictOfInterestType;
  conflictedTeamMembers: UserIdentifier[];
}

interface ReturnToPeerReviewReasons {
  conflictOfInterest: ConflictOfInterest[];
  decisionMadeInError: boolean;
  improperReview: ImproperReview;
}

interface ReturnToPeerReviewDetails {
  reasons: ReturnToPeerReviewReasons;
  comment: string;
}

export interface PeerReviewCycleChecks extends ScreeningChecks {
  peerReviewCycle: {
    returnToPeerReviewDetails: ReturnToPeerReviewDetails;
    questions: Question[];
  };
}

export interface PeerReviewCycleCheckingProcess extends ScreeningProcess {
  checks: PeerReviewCycleChecks;
}
