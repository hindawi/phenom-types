import { Person } from "../person";

export enum Roles {
  /**
   * @param screener - a screener is a checker who make quality assurance
   * before peer review
   */
  screener = "screener",
  /**
   * @param screener - a quality checker is a checker who make quality assurance
   * after peer review
   */
  qualityChecker = "qualityChecker",
  /**
   * @param teamLeader - a team leader is an user who is responsible for the
   * screening or quality checking flow
   */
  teamLeader = "teamLeader",
}

export interface User extends Person {
  isConfirmed: boolean;
  role: Roles;
}

export interface Checker extends User {
  role: Roles.screener | Roles.qualityChecker;
}

export interface TeamLeader extends User {
  role: Roles.teamLeader;
}
