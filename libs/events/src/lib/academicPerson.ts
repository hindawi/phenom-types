import { Person } from "./person";

interface Affiliation {
  name: string;
  ror? : {
    id: string;
  }
}

export interface AcademicPerson extends Person {
  aff?: string; // TODO: Make sure nobody uses this anymore and delete aff field
  affiliation: Affiliation;
  country?: string;
  title?: string;
}
