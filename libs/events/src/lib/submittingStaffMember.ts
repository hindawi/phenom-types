import { EventObject } from "./eventObject";
import { ISODateString } from "./dateType";
import { Person } from "./person";
import { MemberStatuses } from "./memberStatuses";

export interface SubmittingStaffMember extends EventObject, Person {
  assignedDate: ISODateString;
  orcidId: string;
  status: MemberStatuses;
  userId: string;
}
