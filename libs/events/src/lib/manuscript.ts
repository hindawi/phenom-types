import { ArticleType } from "./articleType";
import { Author } from "./author";
import { Editor } from "./editor";
import { ISODateString } from "./dateType";
import { PhenomFile } from "./phenomFile";
import { Review } from "./review";
import { Reviewer } from "./reviewer";
import { SubmittingStaffMember } from "./submittingStaffMember";
import { SourceJournal } from "./sourceJournal";

export interface Manuscript {
  abstract: string;
  acceptedDate: ISODateString;
  articleType: ArticleType;
  authors: Author[];
  conflictOfInterest: string;
  customId: string;
  dataAvailability: string;
  editors: Editor[];
  files: PhenomFile[];
  fundingStatement: string;
  id: string;
  journalId: string;
  qualityChecksSubmittedDate: ISODateString;
  reviewers: Reviewer[];
  reviews: Review[];
  sectionId?: string;
  specialIssueId?: string;
  sourceJournal?: SourceJournal;
  sourceJournalManuscriptId?: string;
  submissionCreatedDate: ISODateString;
  submittingStaffMembers?: SubmittingStaffMember[];
  title: string;
  version: string;
  /**
   * @param preprintValue - Identifier belonging to a preprint server (eg. arXiv,
   * BioRxiv, PrePubMed etc.) usually added by the author in order to get a
   * discount.
   */
  preprintValue?: string;
  linkedSubmissionCustomId?: string;
}
