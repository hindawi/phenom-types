import { AcademicPerson } from "./academicPerson";
import { EventObject } from "./eventObject";
import { ISODateString } from "./dateType";
import { MemberStatuses } from "./memberStatuses";

export interface Author extends EventObject, AcademicPerson {
  assignedDate: ISODateString;
  isCorresponding: boolean;
  isSubmitting: boolean;
  orcidId: string;
  status: MemberStatuses;
  userId: string;
}
