export enum MemberStatuses {
  accepted = "accepted",
  active = "active",
  declined = "declined",
  expired = "expired",
  pending = "pending",
  removed = "removed",
  submitted = "submitted",
  conflicting = "conflicting",
}
