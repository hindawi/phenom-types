import { ISODateString } from "../dateType";

export enum CouponType {
  /**
   * @param MULTIPLE_USE - A coupon that can be applied to multiple invoices
   */
  MULTIPLE_USE = "MULTIPLE_USE",

  /**
   * @param SINGLE_USE - A coupon that can be applied only to one invoice
   */
  SINGLE_USE = "SINGLE_USE",
}

export enum CouponApplicableInvoiceItemType {
  APC = "APC",
}

export interface InvoiceCoupon {
  /**
   * @param applicableToInvoiceItemType - The type of invoice items that
   * coupons can be applied to, eg. `APC`
   */
  applicableToInvoiceItemType: CouponApplicableInvoiceItemType;

  /**
   * @param code - The code associated to the coupon, consisting of 6 to 10
   * alpha numeric uppercase characters
   */
  code: string;

  /**
   * @param couponCreatedDate - Date when the coupon was created
   */
  couponCreatedDate: ISODateString;

  /**
   * @param couponExpirationDate - Expiration date for `MULTIPLE_USE` coupons
   * that have one set
   */
  couponExpirationDate?: ISODateString;

  /**
   * @param reduction - The percentage of the reduction applied by the coupon
   */
  couponReduction: number;

  /**
   * @param type - The type of coupon
   */
  couponType: CouponType;

  /**
   * @param couponUpdatedDate - Last date when the coupon was updated
   */
  couponUpdatedDate: ISODateString;

  /**
   * @param id - The id of the coupon designated in Invoicing
   */
  id: string;
}
