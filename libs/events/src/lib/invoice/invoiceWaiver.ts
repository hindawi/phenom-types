export interface InvoiceWaiver {
  /**
   * @param reduction - The percentage of the reduction applied by the waiver
   */
  waiverReduction: number;

  /**
   * @param type - The type of waiver
   */
  waiverType: string;
}
