export enum PayerType {
  /**
   * @param INDIVIDUAL - The payer is a person, eg. when the author pays
   * himself for the APC
   */
  INDIVIDUAL = "INDIVIDUAL",

  /**
   * @param INSTITUTION - The payer is an organization/institution, eg an
   * University, Research institute, etc. , which can potentially have its
   * VAT deducted, if it has a valid VAT Registration number
   */
  INSTITUTION = "INSTITUTION",
}

export interface InvoicePayer {
  /**
   * @param address - The billing address of the payer
   */
  billingAddress: string;

  /**
   * @param country - The country code of the billing address
   */
  countryCode: string;

  /**
   * @param email - The email of the payer
   */
  email: string;

  /**
   * @param firstName - The first name of payer that is on the payment
   */
  firstName: string;

  /**
   * @param firstName - The last name of payer that is on the payment
   */
  lastName: string;

  /**
   * @param organization - The organization name, if the payer type is `INSTITUTION`
   */
  organization?: string;

  /**
   * @param type - Determine if payer is an individual or institution
   */
  type: PayerType;

  /**
   * @param vatRegistrationNumber - The vat registration number for the institution
   */
  vatRegistrationNumber?: string;
}
