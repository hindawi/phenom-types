import { ISODateString } from "../dateType";

import { InvoiceCosts } from "./invoiceCosts";
import { InvoiceItem } from "./invoiceItem";
import { InvoicePayer } from "./payer";
import { InvoicePayment } from "./payment";

export enum InvoiceStatus {
  /**
   * @param ACTIVE - After the customer confirmed the invoice, meaning
   * waiting for payment
   */
  ACTIVE = "ACTIVE",

  /**
   * @param DRAFT - After the internal object has been created, if the
   * transaction is active waiting for customer to confirm the invoice
   */
  DRAFT = "DRAFT",

  /**
   * @param FINAL - After a resolution has been set: either it was paid, it
   * was waived, or it has been considered bad debt
   */
  FINAL = "FINAL",

  /**
   * @param PENDING - When a user confirms the invoice from a sanctioned country
   */
  PENDING = "PENDING",
}

export interface Invoice {
  /**
   * @param costs - The costs breakdown of the invoice
   */
  costs: InvoiceCosts;

  /**
   * @param creditNoteForInvoice - The ID of the invoice which is credited
   */
  creditNoteForInvoice?: string;

  /**
   * @param erpReference - The ID of invoice from ERP
   */
  erpReference: string;

  /**
   * @param invoiceCreatedDate - Date when invoice is created and has the
   * status `DRAFT` set
   */
  invoiceCreatedDate: ISODateString;

  /**
   * @param invoiceFinalizedDate - Date when the invoice was fully payed and
   * transitioned in the status `FINAL`
   */
  invoiceFinalizedDate: ISODateString;

  /**
   * @param invoiceId - Invoice unique identifier.
   */
  invoiceId: string;

  /**
   * @param invoiceIssueDate - Date when the invoice was confirmed by the payer
   * and transitioned in the status `ACTIVE`
   */
  invoiceIssuedDate: ISODateString;

  /**
   * @param invoiceItems - The invoice items of the invoice with each waiver
   * and coupon applied, if any
   */
  invoiceItems: InvoiceItem[];

  /**
   * @param invoiceItems - Invoice current status, see [[InvoiceStatus]]
   */
  invoiceStatus: InvoiceStatus;

  /**
   * @param isCreditNote - Shows if an invoice is a Credit Note (the reverse
   * of an existing invoice)
   */
  isCreditNote: boolean;

  /**
   * @param lastPaymentDate - Date when the last payment was registered
   */
  lastPaymentDate: ISODateString;

  /**
   * @param manuscriptAcceptedDate - Date when manuscript for invoice is
   * accepted and the corresponding author is notified of the invoice
   */
  manuscriptAcceptedDate: ISODateString;

  /**
   * @param payer - The payer details
   */
  payer: InvoicePayer;

  /**
   * @param payments - All the registered payments done by the payer
   */
  payments: InvoicePayment[];

  /**
   * @param referenceNumber - Reference number has the format
   * `${invoiceNumber}/${yearAccepted}`. It is set once the manuscript
   * is accepted.
   */
  referenceNumber: string;

  /**
   * @param transactionId - Id for the parent transaction of the invoice
   */
  transactionId: string;

  /**
   * @param preprintValue - Identifier belonging to a preprint server (eg. arXiv,
   * BioRxiv, PrePubMed etc.) usually added by the author in order to get a
   * discount.
   */
  preprintValue?: string;

  /**
   * @param reason - used for credit note creation reason (i.e., changed payer details, bad debt etc)
   */
  reason?: string;
}
