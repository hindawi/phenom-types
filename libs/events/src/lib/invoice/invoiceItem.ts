import { InvoiceCoupon } from "./invoiceCoupon";
import { InvoiceWaiver } from "./invoiceWaiver";

export enum InvoiceItemType {
  /**
   * @param APC - Article Processing Charges, payment for all the work done
   * to verify, and publish the article
   */
  APC = "APC",

  /**
   * @param PRINT_ORDER - Payment for a printed copy
   */
  PRINT_ORDER = "PRINT ORDER",
}

export interface InvoiceItem {
  /**
   * @param coupons - List of applied coupons to the invoice item
   */
  coupons: InvoiceCoupon[];

  /**
   * @param id - The id of the invoice item, as defined in Invoicing
   */
  id: string;

  /**
   * @param manuscriptCustomId - Custom id assigned to the manuscript/article
   */
  manuscriptCustomId: string;

  /**
   * @param manuscriptId - The manuscript/article id defined in Invoicing
   * (equivalent to the submission id)
   */
  manuscriptId: string;

  /**
   * @param price - The price of the invoice item
   */
  price: number;

  /**
   * @param type - The type of the invoice item
   */
  type: InvoiceItemType;

  /**
   * @param vatPercentage - The vat percentage for this invoice item
   */
  vatPercentage: number;

  /**
   * @param taCode - TA Code for an invoice item
   */
  taCode: string;

  /**
   * @param taDiscount - TA Discount for an invoice item
   */
  taDiscount: number;

  /**
   * @param waivers - List of applied waivers to the invoice item
   */
  waivers: InvoiceWaiver[];
}
