export interface InvoiceCosts {
  /**
   * @param dueAmount - The amount remaining to be paid by the author/payer
   */
  dueAmount: number;

  /**
   * @param grossApc - APC value before discount
   */
  grossApc: number;

  /**
   * @param netAmount -The amount invoiced to the author/payer
   */
  netAmount: number;

  /**
   * @param netApc - APC-Discount
   */
  netApc: number;

  /**
   * @param paidAmount - The total paid amount by the author/payer
   */
  paidAmount: number;

  /**
   * @param totalDiscount - Total value of discounts, coupons + waivers
   */
  totalDiscount: number;

  /**
   * @param vatAmount - the added VAT amount
   */
  vatAmount: number;
}
