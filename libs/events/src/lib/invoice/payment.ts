import { ISODateString } from "../dateType";

export interface InvoicePayment {
  /**
   * @param foreignPaymentId - Id for the foreign payment
   */
  foreignPaymentId: string;

  /**
   * @param paymentAmount - Final price paid by the payer (including VAT).
   */
  paymentAmount: number;

  /**
   * @param paymentDate - The date when the invoice was payed
   */
  paymentDate: ISODateString;

  /**
   * @param paymentType - Describes the way the payer paid the invoice.
   */
  paymentType: string;
}
