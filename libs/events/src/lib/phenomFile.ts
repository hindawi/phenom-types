import { EventObject } from "./eventObject";

export enum FileType {
  COVER_LETTER = "coverLetter",
  FIGURE = "figure",
  MANUSCRIPT = "manuscript",
  REVIEW_FILE = "reviewFile",
  SUPPLEMENTARY = "supplementary",
}

export interface PhenomFile extends EventObject {
  /**
   * @param fileName - The name of the file provided by the author.
   */
  fileName: string;
  /**
   * @param label - ignore
   */
  label?: string;
  /**
   * @param mimeType - file mimetype
   */
  mimeType: string;
  /**
   * @param originalName - The name of the file provided by the author.
   */
  originalName: string;
  /**
   * @param position - The position of the file intended by the author. Relevant mostly for supplementary files
   */
  position?: string;
  /**
   * @param providerKey - The key for finding the file in the AWS S3 bucket
   */
  providerKey: string;
  /**
   * @param size - File size in bytes
   */
  size: number;
  /**
   * @param type - see [[FileType]]
   */
  type: FileType;
  /**
   * @param url - ignore
   */
  url?: string;
}
