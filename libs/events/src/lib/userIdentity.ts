import { AcademicPerson } from "./academicPerson";
import { EventObject } from "./eventObject";

export interface UserIdentity extends EventObject, AcademicPerson {
  identifier?: string;
  isConfirmed: boolean;
  type: string;
}
