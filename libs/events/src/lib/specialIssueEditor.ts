import { Person } from "./person";
import { SpecialIssueEditorRole } from "./specialIssueEditorRole";

export interface SpecialIssueEditor extends Person {
  role: SpecialIssueEditorRole;
  affiliation: string;
  countryCode: string;
}
