/**
 * This will be populated only for transfer manuscripts. For now we only have this feature for GSW
 * (it is present in Hindawi Review too, but we don't have any source journals for now in db)
 * but in the future we will have some this one in Hindawi as well with some journals from Wiley
 */
export interface SourceJournal {
  name: string;
  pissn?: string;
  eissn?: string;
  publisher?: string;
}
