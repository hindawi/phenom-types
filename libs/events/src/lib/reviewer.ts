import { AcademicPerson } from "./academicPerson";
import { EventObject } from "./eventObject";
import { ISODateString } from "./dateType";
import { MemberStatuses } from "./memberStatuses";

export interface Reviewer extends EventObject, AcademicPerson {
  acceptedDate: ISODateString;
  declinedDate: ISODateString;
  expiredDate: ISODateString;
  fromService: string;
  invitedDate: ISODateString;
  orcidId: string;
  responded: ISODateString;
  status: MemberStatuses;
  userId: string;
}
