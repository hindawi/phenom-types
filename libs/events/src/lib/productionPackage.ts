  import { ISODateString } from "./dateType";

export interface SourceJournal {
  name: string;
}
export interface Manuscript {
  files: ReviewFile[];
  submissionId?: string;
  peerReviewPassedDate: ISODateString;
  specialIssue: SpecialIssue;
  abstract?: string;
  title?: string;
  articleType: {
    name?: string;
  };
  journal: Journal;
  customId?: string;
  preprintValue?: string;
  version?: string;
  section?: Section;
  submittedDate?: ISODateString;
  updated?: ISODateString;
  created?: ISODateString;
  conflictOfInterest?: string;
  dataAvailability?: string;
  fundingStatement?: string;
  sourceJournal?: SourceJournal;
}

export interface Journal {
  email?: string;
  name?: string;
  issn?: string;
  journalPreprints: {
    preprint?: Preprint;
  }[];
  code?: string;
  preprints?: Preprint[];
}

export interface Reviews {
  adminReviews: Review[];
  authorReviews: Review[];
  reviewerReviews: Review[];
  triageEditorReviews: Review[];
  academicEditorReviews: Review[];
  editorialAssistantReviews: Review[];
}

export interface Review {
  id?: string;
  comments: ReviewComment[];
  member: ReviewMember;
  submitted?: string;
  recommendation?: string;
}

export interface ReviewComment {
  type: string;
  content: string;
  files: ReviewFile[];
}

export interface Section {
  name?: string;
}

export interface Preprint {
  type?: string;
}

export interface SpecialIssue {
  id?: string;
  name?: string;
  isCancelled?: boolean;
  cancelReason?: string;
  isActive?: boolean;
  customId?: string;
}

export interface Alias {
  surname?: string;
  givenNames?: string;
  /** The honorary title for this person, e.g. "Dr" */
  title?: string;
  email?: string;
  /**
   * The full string describing the affiliation/institution, e.g.
   * "Department of Computer Science, COMSATS University Islamabad, Abbottabad Campus, Pakistan"  
   */
  aff?: string;
  country?: string;
}
export interface ScreenerAlias extends Alias {
  role?: ScreenerRoles;
}

interface User {
  identities: Identity[];
}

export interface Identity {
  type?: string;
  identifier?: string;
  updated: ISODateString;
}

export interface Author {
  id?: string;
  isCorresponding?: boolean;
  isSubmitting?: boolean;
  alias?: Alias;
  user?: User;
}

export interface AcademicEditor {
  id?: string;
  alias?: Alias;
  user?: User;
}

export interface EditorialAssistant {
  alias?: Alias;
}

export interface ReviewMember {
  responded?: ISODateString;
  created?: ISODateString;
  alias: Alias;
}

export enum ScreenerRoles {
  es = 'es',
  qc = 'qc',
  eqa = 'eqa',
  esLeader = 'esLeader',
  qcLeader = 'qcLeader',
  mcLeader = 'mcLeader',
}

export interface ReviewFile {
  type?: string;
  originalName?: string;
  fileName?: string;
  providerKey?: string;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface FigureFile {}