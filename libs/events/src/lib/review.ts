import { EventObject } from "./eventObject";
import { ISODateString } from "./dateType";
import { PhenomFile } from "./phenomFile";

export interface Comment extends EventObject {
  content: string;
  files: PhenomFile[];
  type: string;
}

export interface Review extends EventObject {
  comments: Comment[];
  recommendation: string;
  decisionInfo?: RejectDecisionInfo;
  reviewerId: string;
  submitted: ISODateString;

  /**
   * @param isValid - Shows if a review can be used for making a recommendation
   * or taking a decision. Some reviews are invalidated because of conflicts
   * of interest.
   */
  isValid?: boolean;
}

enum TransferToAnotherJournalOption {
  NO = "NO",
  YES = "YES",
}

type RejectDecisionInfo = {
  reasonsForRejection: {
    outOfScope: boolean;
    technicalOrScientificFlaws: boolean;
    publicationEthicsConcerns: boolean;
    lackOfNovelty?: boolean;
    otherReasons?: string;
  };
  transferToAnotherJournal:
    | { selectedOption: TransferToAnotherJournalOption.NO }
    | {
        selectedOption: TransferToAnotherJournalOption.YES;
        transferSuggestions: string;
      };
};
