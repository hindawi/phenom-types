import { Editor } from "./editor";
import { EventObject } from "./eventObject";
import { ISODateString } from "./dateType";

export interface SpecialIssue extends EventObject {
  callForPapers: string;
  cancelReason?: string;
  customId: string;
  editors: Editor[];
  endDate: ISODateString;
  isActive: boolean;
  isCancelled: boolean;
  name: string;
  startDate: ISODateString;
}
