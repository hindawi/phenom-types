import { Editor } from "./editor";
import { EventObject } from "./eventObject";
import { SpecialIssue } from "./specialIssue";

export interface JournalSection extends EventObject {
  editors: Editor[];
  name: string;
  specialIssues: SpecialIssue[];
}
