export enum SpecialIssueEditorRole {
  LeadGuestEditor = "lead guest editor",
  GuestEditor = "guest editor",
}
