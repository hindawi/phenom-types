import { EventObject } from "../lib/eventObject";
import { SpecialIssueEditor } from "../lib/specialIssueEditor";
import { ISODateString } from "../lib/dateType";

interface SpecialIssueEvent extends EventObject {
  id: string;
  title: string;
  customId: string;
  acronym: string;
  sectionId?: string;
  sectionTitle?: string;
  journalId: string;
  journalTitle: string;
  journalCode: string;
  submissionUrl: string;
  isCancelled: boolean;
  submissionStartDate: ISODateString;
  submissionEndDate: ISODateString;
  publicationDate: ISODateString;
  description: string[];
  topics: string[];
  editors: SpecialIssueEditor[];
}

export type SpecialIssueAdded = SpecialIssueEvent;
export type SpecialIssueUpdated = SpecialIssueEvent;
export type SpecialIssueCancelled = SpecialIssueEvent;



