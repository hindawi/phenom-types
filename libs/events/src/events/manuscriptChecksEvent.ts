import {
    AuthorProfilesSearchPayload,
    SanctionListCheckDonePayload,
    FileConvertedPayload,
    LanguageCheckedPayload,
    SuspiciousWordsProcessingDonePayload,
    AutomaticChecksDonePayload,
    SimilarityCheckFinishedPayload
  } from "../lib/screening";
  
  export type AuthorProfilesSearchFinished = AuthorProfilesSearchPayload;
  export type SanctionListCheckDone = SanctionListCheckDonePayload;
  export type FileConverted = FileConvertedPayload;
  export type LanguageChecked = LanguageCheckedPayload;
  export type SuspiciousWordsProcessingDone = SuspiciousWordsProcessingDonePayload;
  export type AutomaticChecksDone = AutomaticChecksDonePayload;
  export type SimilarityCheckFinished = SimilarityCheckFinishedPayload;
  