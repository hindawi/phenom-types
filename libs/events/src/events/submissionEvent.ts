import { EventObject } from "../lib/eventObject";
import { ISODateString } from "../lib/dateType";
import { Manuscript } from "../lib/manuscript";

interface SubmissionEvent extends EventObject {
  manuscripts: Manuscript[];
  submissionId: string;
}
interface PersonInfo {
  email: string;
  givenNames: string;
  surname: string;
}
interface QualityCheckInfo {
  date: ISODateString;
  ea: PersonInfo;
  es: PersonInfo;
  esLeaders: PersonInfo[];
  figureFilesCount: number;
  qc: PersonInfo;
  qcLeaders: PersonInfo[];
  refCount: string;
}

export type SubmissionAccepted = SubmissionEvent;
export type SubmissionPackageCreated = SubmissionEvent;
export type SubmissionRevisionSubmitted = SubmissionEvent;
export type SubmissionSubmitted = SubmissionEvent;
export type SubmissionWithdrawn = SubmissionEvent;
export type SubmissionRejected = SubmissionEvent;
export type SubmissionEditedByAdmin = SubmissionEvent; //deprecated
export type SubmissionEdited = SubmissionEvent;
export type SubmissionEditorSuggestionsIngested = SubmissionEvent;

export type SubmissionScreeningPassed = SubmissionEvent;
export type SubmissionScreeningRTCd = SubmissionEvent;
export type SubmissionScreeningReturnedToDraft = SubmissionEvent;
export type SubmissionScreeningPaused = SubmissionEvent;
export type SubmissionScreeningUnpaused = SubmissionEvent;
export type SubmissionScreeningVoid = SubmissionEvent;
export type SubmissionScreeningEscalated = SubmissionEvent;
export type SubmissionScreeningReturnedToChecker = SubmissionEvent;

export type SubmissionQualityChecksSubmitted = SubmissionEvent;
export type SubmissionQualityCheckPassed = SubmissionEvent & QualityCheckInfo;
export type SubmissionQualityCheckFilesRequested = SubmissionEvent;
export type SubmissionQualityCheckRTCd = SubmissionEvent;
export type SubmissionQualityCheckingEscalated = SubmissionEvent;
export type SubmissionQualityCheckingPaused = SubmissionEvent;
export type SubmissionQualityCheckingUnpaused = SubmissionEvent;
export type SubmissionQualityCheckingReturnedToChecker = SubmissionEvent;

export type SubmissionPeerReviewCycleCheckPassed = SubmissionEvent;

export type SubmissionEditorialAssistantAssigned = SubmissionEvent;
export type SubmissionEditorialAssistantReassigned = SubmissionEvent;
export type SubmissionEditorialAssistantRemoved = SubmissionEvent;

export type SubmissionTriageEditorAssigned = SubmissionEvent;
export type SubmissionTriageEditorReassigned = SubmissionEvent;

export type SubmissionAcademicEditorAccepted = SubmissionEvent;
export type SubmissionAcademicEditorRemoved = SubmissionEvent;
export type SubmissionAcademicEditorCancelled = SubmissionEvent;
export type SubmissionAcademicEditorDeclined = SubmissionEvent;
export type SubmissionAcademicEditorInvited = SubmissionEvent;
export type SubmissionAcademicEditorInvitationExpired = SubmissionEvent;

export type SubmissionReviewerAccepted = SubmissionEvent;
export type SubmissionReviewerCancelled = SubmissionEvent;
export type SubmissionReviewerDeclined = SubmissionEvent;
export type SubmissionReviewerInvited = SubmissionEvent;
export type SubmissionReviewerRemoved = SubmissionEvent;
export type SubmissionReviewerInvitationExpired = SubmissionEvent;
export type SubmissionReviewerReportSubmitted = SubmissionEvent;

export type SubmissionRecommendationToPublishMade = SubmissionEvent;
export type SubmissionRecommendationToRejectMade = SubmissionEvent;
export type SubmissionRevisionRequested = SubmissionEvent;
export type SubmissionReturnedToAcademicEditor = SubmissionEvent;
export type SubmissionReviewInvalidated = SubmissionEvent;
