import { EventObject } from "../lib/eventObject";
import { UserIdentity } from "../lib/userIdentity";

interface UserEvent extends EventObject {
  agreeTc: boolean;
  defaultIdentityType: string;
  identities: UserIdentity[];
  isActive: boolean;
  isSubscribedToEmails: boolean;
}

export type UserActivated = UserEvent;
export type UserAdded = UserEvent;
export type UserDeactivated = UserEvent;
export type UserUpdated = UserEvent;
export type UserORCIDAdded = UserEvent;
export type UserORCIDRemoved = UserEvent;
