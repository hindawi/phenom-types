import { ArticleType } from "../lib/articleType";
import { Editor } from "../lib/editor";
import { EventObject } from "../lib/eventObject";
import { ISODateString } from "../lib/dateType";
import { JournalSection } from "../lib/journalSection";
import { SpecialIssue } from "../lib/specialIssue";

interface JournalEvent extends EventObject {
  activationDate: ISODateString;
  articleTypes: ArticleType[];
  code: string;
  editors: Editor[];
  email: string;
  isActive: boolean;
  issn: string;
  name: string;
  publisherName?: string;
  sections: JournalSection[];
  specialIssues: SpecialIssue[];
}

export interface JournalAPCUpdated extends EventObject {
  apc: number;
}

export type JournalAdded = JournalEvent;

export type JournalUpdated = JournalEvent;
export type JournalActivated = JournalEvent;
export type JournalEditorAssigned = JournalEvent;
export type JournalEditorRemoved = JournalEvent;
export type JournalEditorialAssistantAssigned = JournalEvent;

export type JournalSectionAdded = JournalEvent;
export type JournalSectionUpdated = JournalEvent;
export type JournalSectionEditorAssigned = JournalEvent;
export type JournalSectionEditorRemoved = JournalEvent;

export type JournalSpecialIssueAdded = JournalEvent;
export type JournalSpecialIssueUpdated = JournalEvent;
export type JournalSpecialIssueExtended = JournalEvent;
export type JournalSpecialIssueEditorAssigned = JournalEvent;
export type JournalSpecialIssueEditorRemoved = JournalEvent;
export type JournalSpecialIssueOpened = JournalEvent;
export type JournalSpecialIssueDeactivated = JournalEvent;

export type JournalSectionSpecialIssueAdded = JournalEvent;
export type JournalSectionSpecialIssueUpdated = JournalEvent;
export type JournalSectionSpecialIssueExtended = JournalEvent;
export type JournalSectionSpecialIssueEditorAssigned = JournalEvent;
export type JournalSectionSpecialIssueEditorRemoved = JournalEvent;
export type JournalSectionSpecialIssueOpened = JournalEvent;
export type JournalSectionSpecialIssueDeactivated = JournalEvent;
