import { Checker, TeamLeader } from "../lib/screening";
import { EventObject } from "../lib/eventObject";

/**
 * * Interface describing how an event for a checker team looks like.
 */
interface CheckerTeamEvent extends EventObject {
  /**
   * @param checkers - Array of checkers
   */
  checkers: Checker[];
  /**
   * @param journalIds - Journal ids
   */
  journalIds: string[];
  /**
   * @param name - The name of the team
   */
  name: string;

  /**
   * @param teamLeaders - Array of team leaders
   */
  teamLeaders: TeamLeader[];

  /**
   * @param type - The type of the checker team
   */
  type: TeamType;
}

export enum TeamType {
  qualityChecking = "qualityChecking", //a team of quality checkers
  screening = "screening", //a team of screeners
}

/**
 * @param CheckerTeamCreated - This event is emitted after a checker team was created
 */
export type CheckerTeamCreated = CheckerTeamEvent;
/**
 * @param CheckerTeamUpdated - This event is emitted after a checker team was updated
 */
export type CheckerTeamUpdated = CheckerTeamEvent;
