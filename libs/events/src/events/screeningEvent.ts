import { EventObject } from "../lib/eventObject";
import { PeerReviewCycleCheckingProcess } from "../lib/screening";

type PeerReviewCycleCheckingProcessEvent = EventObject &
  PeerReviewCycleCheckingProcess;

export type PeerReviewCycleCheckingProcessSentToPeerReview =
  PeerReviewCycleCheckingProcessEvent;
