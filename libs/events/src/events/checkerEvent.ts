import { Checker } from "../lib/screening";
import { EventObject } from "../lib/eventObject";
import { ISODateString } from "../lib/dateType";

/**
 * * Interface describing how an event for a checker looks like.
 */
interface CheckerEvent extends EventObject {
  /**
   * @param assignationDate - the date when a manuscript was assigned to a checker
   */
  assignationDate: ISODateString;
  /**
   * @param checker - the person who makes the quality check
   */
  checker: Checker;
  submissionId: string;
  /**
   * @param teamId - the id of the team checkers where manuscript was assigned to
   */
  teamId: string;

  assignationType: AssignationType;
}

enum AssignationType {
  automaticAssignmentToTheSameChecker = "automaticAssignmentToTheSameChecker",
  automaticAssignmentToANewChecker = "automaticAssignmentToANewChecker",
  automaticReassignmentToANewChecker = "automaticReassignmentToANewChecker",
  automaticReassignmentToTheSameCheckerFromDifferentTeam = "automaticReassignmentToTheSameCheckerFromDifferentTeam",
  automaticAssignmentToANewCheckerFromSameTeam = "automaticAssignmentToANewCheckerFromSameTeam",
  manualReassignment = "manualReassignment",
}

/**
 * @param ScreenerAssigned - This event is emitted when a screener was assigned to a manuscript
 */
export type ScreenerAssigned = CheckerEvent;
/**
 * @param QualityCheckerAssigned - This event is emitted when a quality checker was assigned to a manuscript
 */
export type QualityCheckerAssigned = CheckerEvent;

export type ScreenerReassigned = CheckerEvent; //deprecated
export type QualityCheckerReassigned = CheckerEvent; //deprecated
