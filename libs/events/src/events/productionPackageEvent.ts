import { ISODateString } from "../lib/dateType";
import { AcademicEditor, Author, EditorialAssistant, FigureFile, Manuscript, Reviews, ScreenerAlias } from "../lib/productionPackage";


export type ProductionPackageQcArchiveRequested = {
  manuscript: Manuscript;
  authors: Author[];
  reviews?: Reviews;
  figureFiles: FigureFile[];
  publisherName?: string;
  academicEditor: AcademicEditor;
  screeners?: ScreenerAlias[];
  editorialAssistant?: EditorialAssistant;
  submittingStaffMember?: EditorialAssistant;
  correspondingEditorialAssistant?: EditorialAssistant;
  finalRevisionDate?: ISODateString;
  submissionQualityCheckPassedDate?: ISODateString;
}
