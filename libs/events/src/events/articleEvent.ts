import { Author } from "../lib/author";
import { EventObject } from "../lib/eventObject";
import { ISODateString } from "../lib/dateType";
import { PhenomFile } from "../lib/phenomFile";
/**
 * Interface describing how an event for a published article looks like.
 */
interface ArticleEvent extends EventObject {
  articleType: string;
  authors: Author[];
  customId: string;
  doi: string;
  files: PhenomFile[];
  journalId: string;
  published: ISODateString; // any date format that can be understood by new Date(str)
  sectionId?: string;
  specialIssueId?: string;
  submissionId: string;
  title: string;
  abstract: string;
  volume: string;
  refCount: number;
  hasSupplementaryMaterials: boolean;
}

interface ArticlePauseEvent {
  created: ISODateString;
  customId: string;
  journalId: string;
}

/**
 * This event is issued by the CMS when an article is published.
 */
export type ArticlePublished = ArticleEvent;
export type ArticleUpdated = ArticleEvent;
export type ArticlePauseIndexing = ArticlePauseEvent;
export type ArticleResumeIndexing = ArticlePauseEvent;
