import { EventObject } from "../lib/eventObject";

export interface EmailAccount{
       name?:string;
       email:string;
}
interface SendEmailEvent extends EventObject {
       sender: EmailAccount;
       recipient: EmailAccount;
       bcc?: EmailAccount[];
       replyTo?:EmailAccount;
       usecase:string;
       templatePlaceholders?:Record<string, string|string[]>; 
       updatedHtmlBodies?: string[]; 
}

export type EmailRequested = SendEmailEvent;