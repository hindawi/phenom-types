import { ISODateString } from "../lib/dateType";
import { Author, Manuscript, PublicationChargeType } from "../lib/eeoLinkPackage";

/**
 * Used to signal that a manuscript must be exported to EEO:Link
 */
type ManuscriptEeoLinkExportRequested = {
  manuscript: Manuscript;
  authors: Author[];
  figureFileCount?: number;
  publisherName?: string;
  finalRevisionDate?: ISODateString;
  publicationChargeType: PublicationChargeType;
  /** 
   * The TA account code that should be charged for this manuscript. 
   * 
   * Note: For EEO:Link integration, this will be mapped to the two meta fields in the JATS xml:
   * <custom-meta-group>
   *   ...
   *   <custom-meta>
   *     <meta-name>institution_code</meta-name>
   *     <meta-value>D080</meta-value>
   *   </custom-meta>"
   *   <custom-meta>
   *     <meta-name>funder-text</meta-name>
   *     <meta-value>D080</meta-value>
   *   </custom-meta>"
   * </custom-meta-group>
   */
  taAccountCode: string,
  
}

export {PublicationChargeType} from "../lib/eeoLinkPackage"
//Provisional
/** This signals that the ZIP package should go into the preaccepted folder and only contain the JATS XML file */
export type ManuscriptPreAcceptanceEeoLinkExportRequested = ManuscriptEeoLinkExportRequested
/** This signals that the ZIP package should go into the accepted folder and include the JATS XML file as well as all figures, supplementary materials, etc. */
export type ManuscriptPostAcceptanceEeoLinkExportRequested = ManuscriptEeoLinkExportRequested


