interface EditorSuggestion {
  /* The teamMember ID from the AcademicEditors team for the requested journal from the review db */
  editorId: string;
  score: number;
}

export type EditorSuggestionListGenerated = {
  submissionId: string;
  manuscriptId: string;
  editorSuggestions: EditorSuggestion[];
};
