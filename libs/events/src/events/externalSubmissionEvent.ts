import { ArticleType } from "../lib/articleType";
import { ISODateString } from "../lib/dateType";
import { EventObject } from "../lib/eventObject";
import { FileType } from "../lib/phenomFile";

interface ExternalSubmissionEvent extends EventObject {
  submissionId: string;
  abstract: string;
  articleType: ArticleType;
  authors: ExternalAuthor[];
  conflictOfInterest: ExternalConflictOfInterestStatement;
  // This will not be sent by rex in the 1st phase and will always be null. 
  // Ideally, we should be ready to receive it at a later point in time
  customId?: string;
  dataAvailability: string;
  files: ExternalFile[];
  fundingStatement: ExternalFundingStatement,
  journalId: string;
  sectionId?: string;
  specialIssueId?: string;
  submissionCreatedDate: ISODateString;
  title: string;
  version: string;
  /**
   * @param preprintValue - Identifier belonging to a preprint server (eg. arXiv,
   * BioRxiv, PrePubMed etc.) usually added by the author in order to get a
   * discount.
   */
  preprintValue?: string;
}

export interface ExternalFundingStatement {
  plainText: string; 
  detailedFundingSources?: ExternalFundingSource[]; 
}

export interface ExternalFundingSource {
  recipientName: string;
  funderName: string;
  funderId?: string; // not sure if should be optional
  awardId?: string; // not sure if should be optional
}


export interface ExternalAuthor extends EventObject {
  isCorresponding: boolean;
  isSubmitting: boolean;
  orcidId?: string; // optional for now as it is not yet present in xml
  
  // from academic person
  affiliation: ExternalAffiliation;
  country?: string;
  title?: string;

  // from person
  email: string;
  givenNames: string;
  surname: string;
}

export interface ExternalAffiliation {
  name: string;
  ror? : {
    id: string;
  }
}

export interface ExternalFile extends EventObject {
  /** TODO: Is this different from originalName? If so, how? */
  fileName: string;
  /**
   * @param mimeType - file mimetype
   */
  mimeType: string;
  /**
   * @param originalFileName - The name of the file provided by the author.
   */
  originalFileName: string;
  /**
   * @param providerKey - The key for finding the file in the AWS S3 bucket
   */
  providerKey: string;
  /**
   * @param size - File size in bytes
   */
  size: number;
  /**
   * @param type - see [[FileType]]
   */
  type: FileType;

}

export type ExternalConflictOfInterestStatement =
  | {
      type: ExternalConflictOfIterestType.TEXT;
      text: string; // No conflict of interest will be a default text: eg: No. The authors declare no conflict of interest.
    }
  | {
      type: ExternalConflictOfIterestType.FILE;
      text: string; // some kind of default text: eg: Please see the attached coi file
      url: string;
    };

export enum ExternalConflictOfIterestType {
  TEXT = 'TEXT',
  FILE = 'FILE'
}


export type ExternalSubmissionSubmittedEvent = ExternalSubmissionEvent;