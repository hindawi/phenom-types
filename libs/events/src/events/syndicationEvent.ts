import {
  CrossrefTarget,
  Database,
  JournalDatabaseStatus,
  Publisher,
  SyndicationTarget,
} from "../lib/syndication";
import { EventObject } from "../lib/eventObject";
import { ISODateString } from "../lib/dateType";

interface IndexerCreatedEvent extends EventObject {
  name: string;
  shortName: string;
}

interface IndexerUpdatedEvent extends IndexerCreatedEvent {
  active: boolean;
  databases: Database[];
  publishers: Publisher[];
}

interface JournalDatabaseCreatedEvent {
  databaseId: string;
  journalId: string;
}

interface JournalDatabaseDeletedEvent {
  databaseId: string;
  journalId: string;
}

interface JournalDatabaseUpdatedEvent extends JournalDatabaseCreatedEvent {
  acceptanceDate: ISODateString;
  acceptanceText: string;
  coverageDate: ISODateString;
  status: JournalDatabaseStatus;
}

interface BatchSyndicatedEvent {
  id: string;
  indexerId: string;
  publisherId: string;
  syndicationTargets: SyndicationTarget[];
  targetDate: ISODateString;
}

/**
 * Interface describing how an event for processed crossrefTargets looks like.
 * We register DOIs to Crossref for every published Article, via an API connection. Each published Article is treated individually and transferred in a separate API request.
 * For every Article, we send a single file via the API connection, the Crossref XML created by Production for every published article
 */
interface CrossrefTargetProcessedEvent {
  /**
   * @param crossrefTargets - the crossrefTargets of the article that was syndicated to Crossref
   */
  crossrefTargets: CrossrefTarget[];
  /**
   * @param customId - the customId of the article that was syndicated to Crossref
   */
  customId: string;
  /**
   * @param id - the id of the article that was syndicated to Crossref
   */
  id: string;
  /**
   * @param targetDate - the targetDate when the syndication to Crossref happened
   */
  targetDate: ISODateString;
}

export type IndexerCreated = IndexerCreatedEvent;
export type IndexerUpdated = IndexerUpdatedEvent;
export type JournalDatabaseCreated = JournalDatabaseCreatedEvent;
export type JournalDatabaseDeleted = JournalDatabaseDeletedEvent;
export type JournalDatabaseUpdated = JournalDatabaseUpdatedEvent;
export type BatchSyndicated = BatchSyndicatedEvent;
export type CrossrefTargetProcessed = CrossrefTargetProcessedEvent;
export type CrossrefTargetResolved = CrossrefTargetProcessedEvent;
export type CrossrefTargetMetadataDepositProcessed =
  CrossrefTargetProcessedEvent;
