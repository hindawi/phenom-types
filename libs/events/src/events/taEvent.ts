import { EventObject } from '../lib/eventObject';

export enum EligibilityType {
  NotEligible = 'NotEligible',
  Discount = 'Discount',
  FullCoverage = 'FullCoverage'
}

export enum EligibilityStatus {
  Pending = 'Pending',
  Resolved = 'Resolved'
}

interface IManuscriptTAEligibilityUpdated extends EventObject {
  data: EligibilityUpdate;
}

/**
 * Eligibility result after the call to WOAD is made,
 * when SubmissionScreeningPassed is received in phenom-taacl service
 **/

type EligibilityUpdate = {
  submissionId: string;
  eligibilityType: EligibilityType;
  eligibilityStatus: EligibilityStatus;
  discounts?: {
    taCode: string;
    currency: string;
    value: number;
  };
  approval?: IFundingApprovalResult;
};

/**
 * FundingApprovalResult contains information from the WOAD api call in the phenom-taacl service
 * that specifies if funding is approved or declined
 **/

interface IFundingApprovalResult {
  customId: string;
  submissionId: string;
  status: string;
  reason: string;
  declineReasonText: string;
  decisionTime: string;
  updatedBy: string;
  fundingStatementText: string;
  overrideFunderStatementText: boolean;
  includeFunderStatementText: boolean;
  absoluteDiscount?: {
    currency: string;
    value: number;
  };
  percentageDiscount?: {
    value: number;
  };
}

export type ManuscriptTAEligibilityUpdated = IManuscriptTAEligibilityUpdated;
export type ManuscriptTAFoundingResponse = IManuscriptTAEligibilityUpdated;
