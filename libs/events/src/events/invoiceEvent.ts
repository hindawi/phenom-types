import { EventObject } from "../lib/eventObject";

import { Invoice } from "../lib/invoice";

type InvoiceEvent = EventObject & Invoice;

export type InvoiceDraftCreated = Omit<
  InvoiceEvent,
  "payments" | "payer" | "manuscriptAcceptedDate" | "reason"
>;

export type InvoiceDraftDeleted = Omit<
  InvoiceEvent,
  "payments" | "payer" | "manuscriptAcceptedDate" | "reason"
>;

export type InvoiceDraftDueAmountUpdated = Omit<
  InvoiceEvent,
  "payments" | "payer" | "manuscriptAcceptedDate" | "reason"
>;

export type InvoiceCreated = Omit<
  InvoiceEvent,
  "payments" | "payer" | "reason"
>;

export type InvoiceConfirmed = Omit<InvoiceEvent, "payments" | "reason">;

export type InvoicePaid = Omit<InvoiceEvent, "reason">;

export type InvoiceFinalized = InvoiceEvent;

export type InvoiceCreditNoteCreated = InvoiceEvent;
