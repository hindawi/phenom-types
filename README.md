# phenom-types
Repo containing shared data types used across Phenom components.

## events
Package containing the structure of the notification events emitted by Phenom components.

#### Publishing
```sh
# The env var export CODEARTIFACT_AUTH_TOKEN should be present in your environment

# in project root
nx build events
npm publish ./libs/events
```

## validator
Package providing runtime validations for types defined in @hindawi/phenom-types

#### Publishing
```sh
# The env var export CODEARTIFACT_AUTH_TOKEN should be present in your environment

# in project root
nx build validator
npm publish ./dist/libs/validator
```


### Setting up CODEARTIFACT_AUTH_TOKEN
```shell
export CODEARTIFACT_AUTH_TOKEN=`aws codeartifact get-authorization-token --domain hindawi --query authorizationToken --output text --profile $AWS_PROFILE`
# you can do this in you sw dev/prod script, in you ~/.zshrc or anywhere you prefer  
```
