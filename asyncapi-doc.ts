import * as tsj from "ts-json-schema-generator";
import * as path from "path";
import * as fs from "fs";
import * as YAML from "yaml";
import * as ts from "typescript";
import * as glob from "glob";

function name(node: ts.TypeAliasDeclaration): string {
  return node.name.escapedText as string;
}

function hasModifier(node: ts.HasModifiers, keyword: ts.SyntaxKind): boolean {
  return (ts.getModifiers(node) ?? []).some( modifier => modifier.kind === keyword)
}

function asTypeAlias(node: ts.Node): ts.TypeAliasDeclaration {
  return node as ts.TypeAliasDeclaration;
}

function exportedTypes(pattern: string): string[] {
  var types: string[] = []
  const fileNames = glob.sync(pattern)

  for (let path of fileNames) {
    const node = ts.createSourceFile(
      'none.ts',
      fs.readFileSync(path, 'utf8'),
      ts.ScriptTarget.Latest
    );

    node.forEachChild(child => {
      if (child.kind === ts.SyntaxKind.TypeAliasDeclaration) {
        let node = asTypeAlias(child)
        if (hasModifier(node, ts.SyntaxKind.ExportKeyword)) {
          types.push(name(node));
        }
      }
    })
  }

  return types;
}

function genMessages(srcPath: string, schemasRef: string) {
  var oneOf = []
  var definitions = {}
  for (let message of exportedTypes(srcPath)) {
    definitions[message] = {
      name: message,
      payload: {
        '$ref': `${schemasRef}#/definitions/${message}`
      }
    }
    oneOf.push({
      '$ref': `#/definitions/${message}`
    })
  }
  return {
    all: {
      oneOf,
    },
    definitions
  }
}

function writeYaml(object: any, filename: string) {
  const parsed = YAML.stringify(object, null, 2);
  fs.writeFile(filename, parsed, (err) => {
      if (err) throw err;
  })
}

function writeMessages(src: string, filename: string, schemasRef: string) {
  writeYaml(genMessages(src, schemasRef), filename);
}

function writeSchemas(src: string, filename: string) {
  const schema = tsj.createGenerator({path: src}).createSchema();
  writeYaml(schema, filename);
}

let srcSchemas = path.join(__dirname, "libs/events/**/*.ts");
let srcEvents = path.join(__dirname, "libs/events/src/events/*.ts");
let outDir = "asyncapi";
let schemasFile = "schemas.yaml";
let schemasPath = path.join(outDir, "schemas.yaml");
let messagesPath = path.join(outDir, "messages.yaml");

writeSchemas(srcSchemas, schemasPath);
writeMessages(srcEvents, messagesPath, schemasFile);
