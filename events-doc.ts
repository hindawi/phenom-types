import { Application, ReflectionKind } from "typedoc";

const app = new Application({
  mode: "file",
  tsconfig: "tsconfig.json",
});

const project = app.convert(app.expandInputFiles(["libs/events"]));

if (!project) process.exit(1);

const toc: string[] = [];

toc.push(
  ...project
    .getReflectionsByKind(ReflectionKind.Interface)
    .map((_interface) => _interface.name)
    .filter((interfaceName) => interfaceName.endsWith("Event")),
);

toc.push(
  ...project
    .getReflectionsByKind(ReflectionKind.TypeAlias)
    .map((_typeAlias) => _typeAlias.name),
);

app.options.setValue("toc", toc);

app.generateDocs(project, "public");
